package wtf.lpc.hourlymusic

import javafx.embed.swing.JFXPanel
import javafx.scene.media.Media
import javafx.scene.media.MediaPlayer
import javafx.util.Duration
import khttp.get
import java.io.File
import java.net.UnknownHostException
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.schedule
import kotlin.system.exitProcess

val musicFiles = mutableMapOf<Weather, MutableMap<Int, File>>()
lateinit var mediaPlayer: MediaPlayer

enum class PeriodOfDay { AM, PM }
enum class Weather { NORMAL, RAIN, SNOW }
data class TwelveHour(val hour: Int, val period: PeriodOfDay)

const val DARK_SKY_KEY = "key"
const val IPSTACK_KEY = "key"

var forcedWeather: Weather? = null

fun twentyFourToTwelve(hour: Int): TwelveHour {
    var period = PeriodOfDay.AM
    var twelveHour = hour
    if (hour >= 12) period = PeriodOfDay.PM
    if (hour >= 13) twelveHour = hour - 12
    if (hour == 0) twelveHour = 12
    return TwelveHour(twelveHour, period)
}

fun prepareMusic(musicDir: File) {
    for (condition in listOf(Weather.NORMAL, Weather.RAIN, Weather.SNOW)) {
        var weatherDir = File(musicDir, condition.toString().toLowerCase())
        if (!weatherDir.exists()) {
            if (condition == Weather.NORMAL) {
                println("Error: No folder found for normal music.")
                exitProcess(1)
            }
            weatherDir = File(musicDir, "normal")
        }
        for (hour in 0..23) {
            val twelveHour = twentyFourToTwelve(hour)
            var file = File(weatherDir, "${twelveHour.hour}${twelveHour.period}.mp3")
            if (!file.exists()) {
                if (condition == Weather.NORMAL) {
                    println("Error: No file found for ${twelveHour.hour} ${twelveHour.period} (normal).")
                    exitProcess(1)
                }
                val normalWeatherDir = File(musicDir, "normal")
                file = File(normalWeatherDir, "${twelveHour.hour}${twelveHour.period}.mp3")
            }
            if (musicFiles.contains(condition)) {
                musicFiles[condition]?.set(hour, file)
            } else {
                musicFiles[condition] = mutableMapOf(hour to file)
            }
        }
    }
}

fun playSong(hour: Int) {
    var weather = Weather.NORMAL
    if (forcedWeather != null) {
        weather = forcedWeather as Weather
    } else {
        try {
            val ipstackResponse = get("http://api.ipstack.com/check?access_key=$IPSTACK_KEY")
            val latitude = ipstackResponse.jsonObject.getDouble("latitude")
            val longitude = ipstackResponse.jsonObject.getDouble("longitude")
            val darkSkyResponse = get("https://api.darksky.net/forecast/$DARK_SKY_KEY/$latitude,$longitude")
            val icon = darkSkyResponse.jsonObject.getJSONObject("currently").getString("icon")
            weather = when (icon) {
                "rain" -> Weather.RAIN
                "snow" -> Weather.SNOW
                else -> Weather.NORMAL
            }
        } catch (e: UnknownHostException) {}
    }
    val file = musicFiles[weather]?.get(hour)
    val song = Media(file?.toURI().toString())
    if (::mediaPlayer.isInitialized) {
        repeat(500) {
            mediaPlayer.volume -= .002
            TimeUnit.MILLISECONDS.sleep(10)
        }
        mediaPlayer.stop()
    }
    val twelveHour = twentyFourToTwelve(hour)
    val conditionString = if (weather == Weather.NORMAL) "" else " (${weather.toString().toLowerCase()}y)"
    println("Now playing: ${twelveHour.hour} ${twelveHour.period}$conditionString")
    TimeUnit.SECONDS.sleep(2)
    mediaPlayer = MediaPlayer(song)
    mediaPlayer.setOnEndOfMedia {
        mediaPlayer.seek(Duration.ZERO)
        mediaPlayer.play()
    }
    mediaPlayer.volume = 0.0
    mediaPlayer.play()
    repeat(500) {
        mediaPlayer.volume += .002
        TimeUnit.MILLISECONDS.sleep(10)
    }
}

fun loop() {
    var previousHour: Int? = null
    Timer().schedule(0, 1) {
        val hour = LocalDateTime.now().hour
        if (previousHour != hour) {
            previousHour = hour
            playSong(hour)
        }
    }
}

fun main(args: Array<String>) {
    if (args.isEmpty()) {
        println("Usage: hourlymusic <path> [weather]")
        exitProcess(1)
    }
    if (args.count() > 2) {
        println("Error: Too many arguments.")
        exitProcess(1)
    }
    if (args.count() == 2) {
        var forced = args[1].toUpperCase()
        forced = when (forced) {
            "CLEAR" -> "NORMAL"
            "RAINY" -> "RAIN"
            "SNOWY" -> "SNOW"
            else -> forced
        }
        if (!listOf("NORMAL", "RAIN", "SNOW").contains(forced)) {
            println("Error: Invalid weather condition.")
            exitProcess(1)
        }
        forcedWeather = Weather.valueOf(forced)
    }
    val musicDir = File(args[0])
    prepareMusic(musicDir)
    JFXPanel()
    loop()
}
